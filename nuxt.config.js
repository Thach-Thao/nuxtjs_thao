export default {
    head: {
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Meta description' }
        ]
    },
    css: [
        '~/assets/css/common.css',
        '~/assets/css/about.css',
        '~/assets/css/faq.css',
        '~/assets/css/privacy.css'
    ],
    plugins: [
      {src:'~/plugins/common.js',ssr:false}
    ],

    build: {
        extend(config, ctx) {},
        vendor:['jquery'],
    }
}